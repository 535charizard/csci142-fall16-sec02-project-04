package queue;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ServiceQueueManager 
{
	/**
	 * This class is the Model class, controlling the multithreaded
	 * project.
	 *  
	 * @ author DeArvis Troutman
	 * 
	 * December 2nd at Midnight
	 * 
	 *The information required to run this class is myRandom,c and
	 *customerdistributorTimer. 
	 */
	private Random myRandom;
	private Controller c;
	private Random customerdistributorTimer;
	//Unsuccessful Attempt for Queue
	//  Total num of Customers
    //	private String numCustomersData = myView.getNumOfCustomers().getText();
    //	private int totalCustomers = Integer.parseInt(numCustomersData);
    //	private Random customerDistributor = new Random(totalCustomers);
    //	private int randCustomers = customerDistributor.nextInt(totalCustomers) + 1;
	
    //	private String serviceTime = myView. getServiceTimeText().getText();
    //	private int service = Integer.parseInt(serviceTime);
    //	private Random helpedTimer = new Random(service);;
 	
    //	private Random leavingCustomers;
    //	private int customerTracker = 0;
    //	private Timer myTimer = new Timer();
    //	
    //	private int getRandNumCustomers;
    //	
    //	TimerTask task = new TimerTask()
    //	{
    //		public void run()
    //		{
		        //Return  The rand number of customers chosen	
    //			int getRandNumCustomers = randCustomers;
    //		}
    //	};
	
	/**
	 * Basic Service Queue Manager
	 * 
	 * @DeArvis Troutman
	 */
	public ServiceQueueManager()
	{
		myRandom = new Random(5);
		// Stores the Value selected in the drop down box here
		// Returns a random number of customers from the max number of customers
		//Stores the value for the service time
	}
		
//	public void start()
//	{
//		myTimer.scheduleAtFixedRate(task, 1000,4000);
//	}
//	
	
	/**
	 *This method returns a randomly selected value.
	 * 
	 * @param queueNum 
	 * @return myRandom.nextInt(5) + 1
	 * 
	 * @DeArvis Troutman
	 */
	public int getNumCustomers(int queueNum)
	{
	    /*	  Creates and place Customers in line
		 * 1.) takes in a total number of customers from JTextArea
		 * 2.) every x amount of time release a random number between
		 * 		0 and the Total Number of Customers.
		 * 3.) make a variable that keeps track of how many customers
		 * 		have been released.
		 * 
		 *      Gets rid of customers that have been helped
		 * 1.) select random  time between 0 and Max Service Time(Finished)
		 * 2.) Start finish timer
		 * 3.) End finish timer once it hits random number
		 * 4.) every time (Finished) pass, get rid of one customer
		 * 5.) Loop over to step 2 and do all again until customers
		 * 		all customers are gone.
		 */
		
		
		return myRandom.nextInt(5) + 1;
	}
}