package queue;

public class main {
    /**
     * This class calls upon the other classes in the MVC, initializes them and
     * displays them.
     * 
     * @ author DeArvis Troutman 
	 * @ author DeArvis Troutman
	 * 
	 * September 28th at Midnight
	 * 
	 * The information required for this method is myController.
     */
	// Properties
    private Controller myController;

    /**
     * This method calls upon the main method, puzzle method and 
     * initializes them.
     * 
     * @param args
     * 
     * @DeArvis Troutman
     */
	public static void main(String[] args) 
	{
		new main();
	}
	/**
	 * This method initializes the controller.
	 * 
	 * @DeArvis Troutman
	 */
	public main()
    {
        this.setController(new Controller());
    }
	public void setController(Controller myController) {
		this.myController = myController;
	}

	public Controller getController() {
		return myController;
	}
}
