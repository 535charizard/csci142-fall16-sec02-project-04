package queue;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Controller implements Runnable {
	/**
	 * This class is connected to the model and the view,
	 *  linking the two with each other and notifying the 
	 *  other when an action or method is implemented.
	 *  
	 * @ author DeArvis Troutman
	 * 
	 * December 2nd at Midnight
	 * 
	 *The information required to run this class is myModel,myView,mySuspended,
	 *myThread and on.
	 */
	
	 ///////////////////
     //  Properties   //
     ///////////////////
     private ServiceQueueManager myModel;
     private View myView;
     private boolean mySuspended;
     private Thread myThread;
     private boolean on = false;
 
    
     ///////////////////
     //    Methods    //
     ///////////////////
    
     /**
      * Controller constructor; view must be passed in since 
      * controller has responsibility to notify view when 
      * some event takes place.
      * 
      * @DeArvis Troutman
      */ 
      public Controller()
      {
    	myModel = new ServiceQueueManager();
    	myView = new View(this);
        myThread = new Thread(this);
 		mySuspended = false;
       }
    
    /**
     * This method stores the values for the total number of customers used
     * in the queue. It then chooses a random number between 0 and the total
     * number of customers and returns it.
     * 
     * @return
     * 
     * @DeArvis Troutman
     */
    public int numCustomers()
    {
    	String numCustomersData = myView.getNumOfCustomers().getText();
		int totalCustomers = Integer.parseInt(numCustomersData);
		Random customerDistributor = new Random(totalCustomers);
		int randCustomers = customerDistributor.nextInt(43) + 1;
		
    	return randCustomers ;
    }
    /**
     * This method displays the customers and tellers 
     * in the queue in the GUI through the view.
     * 
     * 
     * @DeArvis Troutman
     */
     private void displayCustomers(int queue)
	 {
		int numInQueue =  myModel.getNumCustomers(queue);

		myView.setCustomersInLine(queue, numInQueue);
	 }

     /**
      * this Method runs the threads in the Queue.
      * 
      * @DeArvis Troutman
      */
	
    @Override
	public void run() 
	{
		// I need to kill this thread once the specific time has elapsed
		/* 
		*  long start = System.currentTimeMillis();
		*  long end = start + 10*1000; // 10 seconds * 1000 ms/sec
		*  while (System.currentTimeMillis() < end)
		*  {
    	*      //run
		*  }
		*/
    	long start = System.currentTimeMillis();
		long end = start + 10*1000; // 10 seconds * 1000 ms/sec
    	while(System.currentTimeMillis() < end)
    	{
		    try
    	    {
    		    synchronized(this)
    		{
    		    this.updateView();
    		    System.out.println("Synchronization Complete");
    		}
    	    }
    	        catch (InterruptedException e)
    	    {
    		System.out.println("Thread suspended.");
    	}
    	}
	}
	/**
	 * This method stores the number of tellers that the user selects.
	 * Also, it updates the view and makes the threads sleep.
	 * 
	 * @throws InterruptedException
	 * 
	 * @DeArvis Troutman
	 */
	private void updateView()throws InterruptedException
	{
		while(true)
		{
			this.waitWhileSuspended();
			
			try 
			{
				Thread.sleep(200);
				// Gets the value the user selected in the drop down panel
				int myTellers = (int)myView.getjComboBox1().getSelectedItem();
				for(int x = 0; x < myTellers; x++) // For statement displaying the
				{								 //number of customers per table
			        this.displayCustomers(x);
				}
			} 
			catch (InterruptedException e) 
			{
			    e.printStackTrace();
			}
		}
	}
	
	 /**
	  * This method pauses the Thread.
	  * 
	  * @throws InterruptedException
	  * 
	  * @DeArvis Troutman
	  */
	 private void waitWhileSuspended() throws InterruptedException
	    {
	    	while (mySuspended)
	    	{
	    		this.wait();
	    	}
	    }
	    
	    public void suspend()
	    {
	    	mySuspended = true;
	    }
	    
	    public void start()
	    {    
	        try
	        {
	            myThread.start();
	        }
	        catch(IllegalThreadStateException e)
	        {
	            System.out.println("Thread already started");
	        }
	    }
	    	//Attempt to Add Timer to Queue
//	    	long start = System.currentTimeMillis();
//			long end = start + 10*1000; // 10 seconds * 1000 ms/sec
//			boolean starting = true;
//	    	
//			while(System.currentTimeMillis() < end)
//	    	{
//	    	    if(starting == true)
//	    	    {
//	    	        try
//	                {
//	                 myThread.start();
//	                 starting = false;
//	                }
//	        catch(IllegalThreadStateException e)
//	        {
//	            System.out.println("Thread already started");
//	        }
//	    	}
//	    	}
	    
	    
	    
	    public synchronized void resume()
	    {
	    	mySuspended = false;
	    	this.notify();
	    }
	   
	    
    /**
    * This method switches the JButton when pressed from start to pause
    * and pause to start.
    *  
    * @DeArvis Troutman
    */
    public void startPause()
    {
	    //  Stores the Value selected in the drop down box here
        //  int varName = (int)myView.getjComboBox1().getSelectedItem();
        //	myView.setMAX_NUM_OF_TELLERS(varName);
       		
        //  Stores the value for the number of customers inputed from user
        //	String numCustomersData = myView.getNumOfCustomers().getText();
        //	int totalCustomers = Integer.parseInt(numCustomersData);
	    
    	//  Stores the value for the time for the threads to run
        //	String timeRequired = myView.getTimeText().getText();
        //	int time = Integer.parseInt(timeRequired);
 	    
    	//  Stores the value for the service time
        //	String serviceTime = myView. getServiceTimeText().getText();
        //	int service = Integer.parseInt(serviceTime);
    	
    	if(on == true)
        {
	        if (mySuspended)
	        {
		        this.resume();
		        System.out.println("resuming");
		        System.out.println(mySuspended);
	        }
	
	        else 
	        {
		        this.suspend();
		        System.out.println("pausing");
		        System.out.println(mySuspended);
		        myView.getCashierText().setText("...");
	        }
        }

        if(on == false)
	    {
	        this.start();
	        on = true;
	        System.out.println("Starting");
	        myView.getCashierText().setText("...");
	    }
	    myView.changeStartPause();   // This swaps the start and pause button
    }
    /**
     * This Method Allows the user to click on the Teller and Prints
     * The statistics for the Teller.
     * 
     * @DeArvis Troutman
     */
    public void display()
    {
    	//This will retrieve line separator.
    	String newLine = System.getProperty("line.separator");
    	String Info = new String("Total served:"
    			+ newLine
    			+ "Total Service Time:"
    			+ newLine
    			+ "Total Idle Time:"
    			+ newLine
    			+ "Total Wait Time:"
    			+ newLine
    			+ "Avg Served: "
    			); 
    	myView.getCashierText().setText(Info);
    	System.out.println("The Method Works!!");
    	
  
    }
}
	
	
	
	
	

