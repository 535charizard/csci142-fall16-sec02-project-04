package queue;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.lang.reflect.Method;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicOptionPaneUI.ButtonActionListener;

public class View  
{
 /**
     *This class displays all of the buttons, JLabels and JTextArea's to the GUI.
     *THis is where the GUI and visual aspects of the project are created.
     * 
     * @ author DeArvis Troutman 
	 * @ author DeArvis Troutman
	 * 
	 * December 2nd at Midnight
	 * 
	 *The information required to run this class is flowlayout, frame, statistics,overallStats,
	 *cashierStats, generateTime,numOfCustomersText, numOfTellers,serviceTime,
	 *customerOverFlow,northQueue,southQueue,statsText,cashierText,timeText,
	 *numOfCustomers, serviceTimeText,customerInfo1,customerInfo2,customerInfo3,
	 *customerInfo4,customerInfo5,servedInfo1,servedInfo2,servedInfo3,servedInfo4,
	 *servedInfo5,startPause,jp,jp2,dropdown,JComboBox1,TELLER_WIDTH,TELLER_HEIGHT,
	 *TELER_IMG,FACE_IMG,CUSTOMER_WIDTH,CUSTOMER_HEIGHT,ROW_1,MAX_PEOPLE_IN_LINE,
	 *MAX_NUM_OF_TELLERS, myScaledImage,mySimPanel,myStartPauseListener,myCustomer,
	 *myTeller,
	 */

	// GUI JLAbels & JPanels
	private LayoutManager flowLayout = new FlowLayout(20,60,10);
	private JFrame frame = new JFrame("Queue Simulation");
	private JPanel statistics = new JPanel();
	private JLabel overallStats = new JLabel("  Overall Stats: ");
	private JLabel cashierStats = new JLabel("  Individual Cashier Stats: ");
	private JLabel generateTime = new JLabel("  Generate Time(miliseconds): ");
	private JLabel numOfCustomersText = new JLabel("  # Customers");
	private JLabel numOfTellers = new JLabel("  # Tellers");
	private JLabel serviceTime = new JLabel("  Max Service Time");
	private JLabel customerOverFlow = new JLabel("  Customer OverFlow");
	private JLabel numServed = new JLabel("  # Served");
	private JPanel northQueue = new JPanel();
	private JPanel southQueue = new JPanel();
	
	//JTextArea's and JButton
	private JTextArea statsText = new JTextArea("...");
	private JTextArea cashierText = new JTextArea("...");
	private JTextArea timeText = new JTextArea("  Input Here...");
	private JTextArea numOfCustomers = new JTextArea("  Input Here...");
	private JTextArea serviceTimeText = new JTextArea("  Input Here...");
	private JTextArea customerInfo1 = new JTextArea(" +0 ");
	private JTextArea customerInfo2 = new JTextArea(" +0 ");
	private JTextArea customerInfo3 = new JTextArea(" +0 ");
	private JTextArea customerInfo4 = new JTextArea(" +0 ");
	private JTextArea customerInfo5 = new JTextArea(" +0 ");
	private JTextArea servedInfo1 = new JTextArea(" 0 ");
	private JTextArea servedInfo2 = new JTextArea(" 0 ");
	private JTextArea servedInfo3 = new JTextArea(" 0 ");
	private JTextArea servedInfo4 = new JTextArea(" 0 ");
	private JTextArea servedInfo5 = new JTextArea(" 0 ");
	private JButton startPause = new JButton("Start");
    
	//JScrollPane's
	private JScrollPane jp;
	private JScrollPane jp2;
	
	// Array of type int and JComboBox
	private int[] dropDown = {1,2,3,4,5};
	private JComboBox<Integer> jComboBox1 = new JComboBox<Integer>();	
	
	//Constants
	private final int TELLER_WIDTH = 75;
	private final int TELLER_HEIGHT = 85;
	private final String TELLER_IMG = "images/cashier.png";
	private final String FACE_IMG = "images/face.jpg";
	private final int CUSTOMER_WIDTH = 50;
	private final int CUSTOMER_HEIGHT = 50;
	private final int ROW_1 = 460;
	private final int MAX_PEOPLE_IN_LINE = 5;
	private int MAX_NUM_OF_TELLERS = 5;
	
	//Data Members
	private Image myScaledImage;
	private JPanel mySimPanel = new JPanel();
	private ButtonListener myStartPauseListener;
	private ButtonListener displayInfoListener;
	private JLabel [][] myCustomer;
	private JLabel [] myTeller;

	//Button Beneath Tellers that display text on GUI
	private JButton tellerButton = new JButton("1");
	private JButton tellerButton2 = new JButton("2");
	private JButton tellerButton3 = new JButton("3");
	private JButton tellerButton4 = new JButton("4");
	private JButton tellerButton5 = new JButton("");
private JButton b = new JButton("B");
	/**
	 * This method takes in the controller and updates the GUI with components
	 * connecting itself with the controller which links the view and model.
	 * 
	 * @param controller
	 * 
	 * @DeArvis Troutman
	 */
	public View(Controller controller)
	{
	    frame.setSize(670, 650);
		frame.setLayout(new BorderLayout());
		northQueue.setLayout(flowLayout);
		southQueue.setLayout(flowLayout);
		
			
		statistics.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		associateListeners(controller);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		/////////////////////////////////////////////////////////////////////
		////     GUI JButtons, JTextAreas's, ScrollPane, JComboBox, and GridBagLayout   ////
		/////////////////////////////////////////////////////////////////////
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 1;
		statistics.add(overallStats,c); 
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 100;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 2;
		c.insets = new Insets(10,0,0,0);  //top padding
		jp = new JScrollPane(statsText);
		statistics.add(jp,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 3;
		c.insets = new Insets(0,0,0,0);  //top padding
		statistics.add(cashierStats,c);

		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 100;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 4;
		c.insets = new Insets(10,0,0,0);  //top padding
		jp2 = new JScrollPane(cashierText);
		statistics.add(jp2,c); // Adding JPanel to the statistics
	    
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 5;
		c.insets = new Insets(0,0,0,0);  //top padding
		statistics.add(generateTime,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 6;
		c.insets = new Insets(10,0,0,0);  //top padding
		statistics.add(timeText,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 7;
		c.insets = new Insets(10,0,0,0);  //top padding
		statistics.add(numOfCustomersText,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 8;
		c.insets = new Insets(10,0,0,0);  //top padding
		statistics.add(numOfCustomers,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 9;
		c.insets = new Insets(10,0,0,0);  //top padding
		statistics.add(numOfTellers,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 10;
		c.insets = new Insets(10,0,0,0);  //top padding  
		for(int t = 0; t < dropDown.length; t++)
		{
			jComboBox1.addItem(dropDown[t]);	
		}
        statistics.add(jComboBox1,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 11;
		c.insets = new Insets(10,0,0,0);  //top padding
		statistics.add(serviceTime,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.0;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 12;
		c.insets = new Insets(10,0,0,0);  //top padding
		statistics.add(serviceTimeText,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 0;      //make this component tall
		c.weightx = 0.5;
		c.gridwidth = 1;
		c.gridx = 0;
		c.gridy = 13;
		c.insets = new Insets(10,0,0,0);  //top padding
		statistics.add(startPause,c);
		
		
		northQueue.add(customerInfo1);
		northQueue.add(customerInfo2);
		northQueue.add(customerInfo3);
		northQueue.add(customerInfo4);
		northQueue.add(customerInfo5);
		customerOverFlow.setBorder(new EmptyBorder(0, 0, 0, 0));

		northQueue.add(customerOverFlow);
		
		southQueue.add(servedInfo1);
		southQueue.add(servedInfo2);
		southQueue.add(servedInfo3);
		southQueue.add(servedInfo4);
		southQueue.add(servedInfo5);
		southQueue.add(numServed);
		
	    /////////////////////////////////////////////////////////
	    // 		Customers and Tellers picture organization     //
	    /////////////////////////////////////////////////////////
		
	    Image face = Toolkit.getDefaultToolkit().getImage(FACE_IMG);
	    myScaledImage = face.getScaledInstance(CUSTOMER_WIDTH,CUSTOMER_HEIGHT,Image.SCALE_SMOOTH);
	    mySimPanel.setBorder(BorderFactory.createLoweredBevelBorder());
	    mySimPanel.setLayout(null);
	
	
	    //Teller locations, TellerButtons that are Transparent
	    myTeller = new JLabel[MAX_NUM_OF_TELLERS];
	    tellerButton.setSize(TELLER_WIDTH , TELLER_HEIGHT);
	    tellerButton.setLocation(10 + (CUSTOMER_WIDTH*2*0), ROW_1);
	    tellerButton.setOpaque(false);
	    tellerButton.setContentAreaFilled(false);
	    tellerButton.setBorderPainted(false);
	    
	    tellerButton2.setSize(TELLER_WIDTH , TELLER_HEIGHT);
	    tellerButton2.setLocation(10 + (CUSTOMER_WIDTH*2*1), ROW_1);
	    tellerButton2.setOpaque(false);
	    tellerButton2.setContentAreaFilled(false);
	    tellerButton2.setBorderPainted(false);
	    
	    tellerButton3.setSize(TELLER_WIDTH , TELLER_HEIGHT);
	    tellerButton3.setLocation(10 + (CUSTOMER_WIDTH*2*2), ROW_1);
	    tellerButton3.setOpaque(false);
	    tellerButton3.setContentAreaFilled(false);
	    tellerButton3.setBorderPainted(false);
	    
	    tellerButton4.setSize(TELLER_WIDTH , TELLER_HEIGHT);
	    tellerButton4.setLocation(10 + (CUSTOMER_WIDTH*2*3), ROW_1);
	    tellerButton4.setOpaque(false);
	    tellerButton4.setContentAreaFilled(false);
	    tellerButton4.setBorderPainted(false);
	    
	    tellerButton5.setSize(TELLER_WIDTH , TELLER_HEIGHT);
	    tellerButton5.setLocation(10 + (CUSTOMER_WIDTH*2*4), ROW_1);
	    tellerButton5.setOpaque(false);
	    tellerButton5.setContentAreaFilled(false);
	    tellerButton5.setBorderPainted(false);
	    
	   // tellerButton.setVisible(false);
	    for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
	    {
		    myTeller[i] = new JLabel(new ImageIcon(TELLER_IMG));
		    myTeller[i].setSize(TELLER_WIDTH , TELLER_HEIGHT);
		    myTeller[i].setLocation(10 + (CUSTOMER_WIDTH*2*i), ROW_1);
		    
		   
		    
		 
	 	    myTeller[i].setVisible(true);
	 	    mySimPanel.add(tellerButton);
	 	    mySimPanel.add(tellerButton2);
	 	    mySimPanel.add(tellerButton3);
	 	    mySimPanel.add(tellerButton4);
	 	    mySimPanel.add(tellerButton5);
	 	    mySimPanel.add(myTeller[i]);
		    
		 }

	    //Customer Lines
	    myCustomer = new JLabel[MAX_NUM_OF_TELLERS][MAX_PEOPLE_IN_LINE];
	    for(int i = 0; i < MAX_NUM_OF_TELLERS; i++)
	    {
		    for(int j = 0; j < MAX_PEOPLE_IN_LINE; j++)
		    {
			    myCustomer[i][j] = new JLabel();
			    myCustomer[i][j].setSize(CUSTOMER_WIDTH , CUSTOMER_HEIGHT);
			    myCustomer[i][j].setLocation(10 + (CUSTOMER_WIDTH*2*i), 325 - (50*j));
			    myCustomer[i][j].setVisible(true);
			    mySimPanel.add(myCustomer[i][j]);
		    }
	    } 
	
	    //Background
	    JLabel bg;
	    bg = new JLabel(new ImageIcon("images/background.png"));
	    bg.setSize(500, 600);
	    bg.setLocation(0, 0);
	    mySimPanel.add(bg);
	
	    frame.add(statistics,BorderLayout.EAST);
		frame.add(northQueue,BorderLayout.NORTH);
		frame.add(southQueue,BorderLayout.SOUTH);
		frame.add(mySimPanel, BorderLayout.CENTER);
		frame.setResizable(true);
		frame.setVisible(true);
	}
	//////////////////////////////////////////
	//            Methods                   //
	//////////////////////////////////////////
	public JTextArea getCashierText() 
	{
		return cashierText;
	}

	public void setCashierText(JTextArea cashierText)
	{
		this.cashierText = cashierText;
	}	
	public JTextArea getTimeText() 
	{
		return timeText;
	}


	public void setTimeText(JTextArea timeText)
	{
		this.timeText = timeText;
	}
	
	public JTextArea getNumOfCustomers() 
	{
		return numOfCustomers;
	}


	public JTextArea getServiceTimeText() 
	{
		return serviceTimeText;
	}

	public void setServiceTimeText(JTextArea serviceTimeText) 
	{
		this.serviceTimeText = serviceTimeText;
	}

	public void setNumOfCustomers(JTextArea numOfCustomers) 
	{
		this.numOfCustomers = numOfCustomers;
	}

    public JLabel getNumOfTellers() 
	{
		return numOfTellers;
	}

    public void setNumOfTellers(JLabel numOfTellers) 
    {
		this.numOfTellers = numOfTellers;
	}


	public JComboBox getjComboBox1() 
	{
		return jComboBox1;
	}


	public void setjComboBox1(JComboBox jComboBox1) 
	{
		this.jComboBox1 = jComboBox1;
	}
	
	public int getMAX_NUM_OF_TELLERS() 
	{
		return MAX_NUM_OF_TELLERS;
	}


	public void setMAX_NUM_OF_TELLERS(int mAX_NUM_OF_TELLERS) 
	{
		MAX_NUM_OF_TELLERS = mAX_NUM_OF_TELLERS;
	}
    
	public JTextArea getStatsText() 
	{
		return statsText;
	}

	public void setStatsText(JTextArea statsText) 
	{
		this.statsText = statsText;
	}

	public void changeStartPause() 
	{
		if(startPause.getText().equals("Pause"))
		{
			startPause.setText("Start");
		}
		else
		{
			startPause.setText("Pause");
		}
	}
	/** This method takes in the queue and numInLine
    *  display on the console the queue and number of customers in
	*  line. Also, set's certain pictures equall to a customer
    *  and teller.
    * 
	* @param queue,numInLine
	* 
	* @DeArvis Troutman
	*/
	public void setCustomersInLine(int queue, int numInLine)
    {
	    System.out.println("Queue: " + queue + "  numInLine = " + numInLine);
	    System.out.println("Time in miliseconds : " + System.currentTimeMillis());
		myTeller[queue].setIcon(new ImageIcon(TELLER_IMG));
			
		for(int i = 0; i < MAX_PEOPLE_IN_LINE; i++)
		{
		    myCustomer[queue][i].setVisible(false);
		}
		try
		{
		    for(int i = 0; i < numInLine && i < MAX_PEOPLE_IN_LINE; i++)
		    {
			    myCustomer[queue][i].setVisible(true);
				myCustomer[queue][i].setIcon(new ImageIcon(myScaledImage));
			} 
	    }
		catch (NullPointerException e)
		{
				
		}
    }
    /**
	* This method links the JButtons with the ButtonListener and mouseListener.
	* It it correlates the designated buuton with the designated method in the 
	* controller class. 
	* 
	*@param controller
	* 
	* @DeArvis Troutman
	*/
	public void associateListeners(Controller controller)
	{
        Class<? extends Controller> controllerClass;
        Method clearMethod,
               solveMethod,
        	   startPauseMethod,
        	   tellerButtonMethod; //Teller Display
       
        controllerClass = controller.getClass();
        
        startPauseMethod = null;
        clearMethod = null; 
        solveMethod = null;
        tellerButtonMethod = null;   //Teller Display
     
   
        //  Associate method names with actual methods to be invoked
        try
        {
           startPauseMethod = 
               controllerClass.getMethod("startPause", (Class<?>[])null);
           
           tellerButtonMethod =
        	   controllerClass.getMethod("display", (Class<?>[])null);
        }
        catch(NoSuchMethodException exception)
        {
           String error;
           error = exception.toString();
           System.out.println(error);
         
        }
        catch(SecurityException exception)
        {
           String error;
           error = exception.toString();
           System.out.println(error);
        }
        
        
        myStartPauseListener = 
    			new ButtonListener(controller, startPauseMethod, null);
    	
        displayInfoListener =
        		new ButtonListener(controller, tellerButtonMethod, null);
        
        startPause.addMouseListener(myStartPauseListener);
      
       tellerButton.addMouseListener(displayInfoListener);
       tellerButton2.addMouseListener(displayInfoListener);
       tellerButton3.addMouseListener(displayInfoListener);
       tellerButton4.addMouseListener(displayInfoListener);
       tellerButton5.addMouseListener(displayInfoListener);
        

	}


	

}
